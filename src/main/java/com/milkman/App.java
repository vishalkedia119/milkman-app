package com.milkman;

import org.apache.http.client.HttpClient;
import org.skife.jdbi.v2.DBI;
import com.codahale.metrics.jdbi.InstrumentedTimingCollector;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.milkman.resource.MainResource;
import com.serviceenabled.dropwizardrequesttracker.RequestTrackerBundle;
import com.serviceenabled.dropwizardrequesttracker.RequestTrackerConfiguration;
import com.test.common.async.Executor;
import com.test.common.http.HttpClientBuilder;
import com.test.common.http.RequestTrackerHttpProcessor;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class App extends Application<AppConfig> {

  public static void main(String[] args) throws Exception {
    new App().run("server", args[0]);
  }

  @Override
  public void initialize(Bootstrap<AppConfig> bootstrap) {
    super.initialize(bootstrap);
    bootstrap.addBundle(new RequestTrackerBundle<AppConfig>() {

      @Override
      public RequestTrackerConfiguration getRequestTrackerConfiguration(AppConfig config) {
        return config.getRequestTrackerConfiguration();
      }

    });
  }

  @Override
  public void run(AppConfig config, Environment env) throws Exception {
    env.lifecycle().manage(new Managed() {

      @Override
      public void start() throws Exception {

      }

      @Override
      public void stop() throws Exception {
        Executor.shutDown();
      }

    });
//    final DBIFactory dbiFactory = new DBIFactory();
//    final DBI dbiReadWrite = dbiFactory.build(env, config.getDbReadWrite(), "dbReadWrite");
//    dbiReadWrite.setTimingCollector(new InstrumentedTimingCollector(env.metrics()));
//    final DBI dbiRead = dbiFactory.build(env, config.getDbReadWrite(), "dbRead");
//    dbiRead.setTimingCollector(new InstrumentedTimingCollector(env.metrics()));
    
    final HttpClient httpClient =
        new HttpClientBuilder(env).using(new RequestTrackerHttpProcessor())
            .using(config.getHttpClientConfig()).build("http-client");
    
    env.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    env.jersey().register(MainResource.class);
  }

}
