package com.milkman;

import java.util.List;
import java.util.Map;
import com.serviceenabled.dropwizardrequesttracker.RequestTrackerConfiguration;
import io.dropwizard.Configuration;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppConfig extends Configuration {
  private DataSourceFactory dbReadWrite;
  private DataSourceFactory dbRead;
  private HttpClientConfiguration httpClientConfig;
  private Map<String, List<String>> clientApps;
  private RequestTrackerConfiguration requestTrackerConfiguration =
      new RequestTrackerConfiguration();
}
